from django.conf.urls import patterns, url, include


from api.routers import HybridRouter
from api.views import TagViewSet, CurrentUserView, BlogListView, BlogDetailView, UserViewSet


#router = routers.DefaultRouter()
router = HybridRouter()
#router.register(r'blogs', BlogViewSet)
router.register(r'tags', TagViewSet)
router.register(r'users', UserViewSet)
router.add_api_view("current-user", url(r'^user/current/$', CurrentUserView.as_view(), name='api-current-user'))
router.add_api_view("blog-list", url(r'^blogs/$', BlogListView.as_view(), name='api-blog-list'))



urlpatterns = patterns('api.views',
    url(r'^', include(router.urls)), 
    url(r'^blogs/(?P<pk>\d+)[/]?$', BlogDetailView.as_view())
    
)
