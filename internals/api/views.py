from django.shortcuts import render
from django.contrib.auth.models import User

#import django_filters
from rest_framework import status, viewsets
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import filters
from rest_framework.generics import ListAPIView, RetrieveAPIView

from blog.models import Blog, Tag
from blog.serializers import BlogSerializer, TagSerializer, UserSerializer


VALID_FILTER_FIELDS = {'blog' : ['featured', 'author', 'tag']}
FILTER_REFERENCES = {'blog': {'author' : 'author__username', 'tag': 'tags__id'}}

class BlogListView(ListAPIView):
    serializer_class = BlogSerializer
    permission_classes = (IsAdminUser,)

    def get_queryset(self):
        queryset =  Blog.objects.all()
        author = self.request.QUERY_PARAMS.get('author', None)
        if author is not None:
            queryset = queryset.filter(author__username=author)

        tag = self.request.QUERY_PARAMS.get('tag', None)
        if tag is not None:
            queryset = queryset.filter(tags__id=tag)

        featured = self.request.QUERY_PARAMS.get('featured', None)
        if featured is not None:
            queryset = queryset.filter(featured=featured)

        return queryset

class BlogDetailView(RetrieveAPIView):
    serializer_class = BlogSerializer
    permission_classes = (IsAdminUser,)
    model = Blog

# 
class CurrentUserView(APIView):
    def get(self, request):
        serializer = UserSerializer(request.user)
        return Response(serializer.data)

class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    http_method_names = ('get',)
    permission_classes = (IsAdminUser,)

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ('get',)
    permission_classes = (IsAdminUser,)


"""
@api_view(['GET', 'POST'])
def blog_list(request):

    if request.method == 'GET':
        blogs = Blog.objects.all()
        serializer = BlogSerializer(blogs)
        return Response(serializer.data)
    elif request.method == 'POST':
        pass


@api_view(['GET'])
def blog_detail(request, pk):
    try:
        blog = Blog.objects.get(pk=pk)
    except Blog.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET' :
        serializer = BlogSerializer(blog)
        return Response(serializer.data)
"""
#class BlogFilter(django_filters.FilterSet):
#    class Meta:
#        model = Blog
#    fileds = ['author__username', 'tags__id']

# class BlogViewSet(viewsets.ModelViewSet):
#     filter_backends = (filters.DjangoFilterBackend,)
#     queryset = Blog.objects.all()
#     serializer_class = BlogSerializer
#     http_method_names = ('get',)
#     permission_classes = (IsAdminUser,)
#     #filter_class = BlogFilter
#     filter_fields = ('featured',)
# def filter_val(model, field, get_dict):
#     valid_models = ['blog']
#     valid_filters = {'blog' : ['featured', 'author', 'tag']}
#     valid_filter_values = {'featured': {'true':True, 'True':True, 'false':False, 'False':False, '1':True, '0':False}}
#     if model in valid_models:
#         #model is valid
#         if field in valid_filters[model]:
#             #filed is valid
#         else:
#             return None
#     else :
#         return None

#     def get_queryset(self):
#         if self.request.GET:
#             #clean up request.get and extract only those in valid filter fileds
#             #filter_fields = [e for e in self.request.GET.keys() if e in VALID_FILTER_FIELDS['blog']]
#             filter_fields = filter((lambda x: True if(x in VALID_FILTER_FIELDS['blog']) else False), self.request.GET.keys())
#             filter_dict = self.request.GET.copy()
#             for key in filter_dict:
#                 if key not in filter_fields:
#                     filter_dict.pop(key)
#             import pdb
#             pdb.set_trace()
#             #use filter_fields and replace those with filter references if they exist. eg. author be replaced by author__username
#             return Blog.objects.filter(**filter_dict)

#             #filter_keys = map((lambda x: x if (not x in FILTER_REFERENCES['blog']) else FILTER_REFERENCES['blog'][x]), filter_fields)
#         else:
#             return Blog.objects.all()
