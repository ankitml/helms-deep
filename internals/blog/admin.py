from django.contrib import admin
from blog.models import Blog, Tag

# Register your models here.
class BlogAdmin(admin.ModelAdmin):
    change_form_template = 'admin_change_form.html'
    prepopulated_fields = {"slug": ("title",)}

    def get_queryset(self, request):
        """Limit blogs to those that belong to the request's user."""
        qs = super(BlogAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            # It is mine, all mine. Just return everything.
            return qs
        # Now we just add an extra filter on the queryset and
        # we're done. Assumption: Page.owner is a foreignkey
        # to a User.
        return qs.filter(author=request.user)

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ['author']
        if not request.user.is_superuser:
            self.exclude.append('featured')
        return super(BlogAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        #This parameter will be True if this is a change on an existing model, it is False if the model is a newly created instance.
        if not change:
            obj.author = request.user
            obj.save()
        else :
            obj.save()

admin.site.register(Blog, BlogAdmin)
admin.site.register(Tag)
