# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_blog_featured'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='type_of_blogpost',
            field=models.CharField(default=b'article', max_length=10, choices=[(b'article', b'article'), (b'link', b'link'), (b'video', b'video'), (b'quote', b'quote')]),
            preserve_default=True,
        ),
    ]
