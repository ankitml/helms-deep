from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe 

# Create your models here.
class Tag(models.Model):
    name = models.CharField(max_length=40)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)

class Blog(models.Model):
    ARTICLE = 'article'
    LINK = 'link'
    VIDEO = 'video'
    QUOTE = 'quote'
    Types = (
        (ARTICLE, 'article'),
        (LINK, 'link'),
        (VIDEO, 'video'),
        (QUOTE, 'quote'),
    )
    title = models.CharField(max_length=100, unique=True)
    type_of_blogpost = models.CharField(max_length=10, choices=Types, default=ARTICLE)
    slug = models.SlugField(max_length=100, null=True, blank=True, unique=True)
    tldr = models.CharField(max_length=300)
    body = models.TextField(blank=True, null=True)
    posted = models.DateField(auto_now_add=True)
    tags = models.ManyToManyField(Tag)
    author = models.ForeignKey(User, null=True, blank=True)
    featured = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('posted',)

    def get_html_body(self):
        return mark_safe(self.body)


