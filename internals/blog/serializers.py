from django.contrib.auth.models import User


from rest_framework import serializers


from blog.models import Blog, Tag


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')

class BlogSerializer(serializers.ModelSerializer):

    class Meta:
        model = Blog
    body = serializers.Field(source='get_html_body')
    tags = TagSerializer(many=True, read_only=True)
    author = UserSerializer()

