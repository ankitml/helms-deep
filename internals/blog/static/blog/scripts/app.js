//initialize the app
var blogApp = angular.module('blogApp', ['ngRoute']);

blogApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});

blogApp.filter('iframe', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
});
blogApp.config(function($interpolateProvider, $routeProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');

    $routeProvider.
      when('/', {
        templateUrl: '../static/routes/list.html',
        controller: 'blogList'
      }).
      when('/posts/:filter/:filterId', {
        templateUrl: '../static/routes/list.html',
        controller: 'blogList'
      }).
      when('/post/:postId', {
        templateUrl: '../static/routes/single.html',
        controller: 'blogDetail'
      }).
      when('/link/:postId', {
        templateUrl: '../static/routes/link.html',
        controller: 'blogDetail'
      }).
      otherwise({
        redirectTo: '/'
      });
});

blogApp.constant('baseURL','/api/');

blogApp.factory('apiFactory', function(baseURL, $http) {
    return {
        blogList : function(filter, filterId) {
            if(typeof(filter) == "undefined" && typeof(filterId) =="undefined") {
                var url = baseURL + "blogs/";
            } else {
                var url = baseURL + "blogs/?" + filter + "=" + filterId;
            }
            return $http.get(url);
        },
        currentUser : function() {
            var url = baseURL + "user/current/";
            return $http.get(url);
        },
        blogDetail : function(id) {
            var url = baseURL + "blogs/" + id + "/";
            return $http.get(url);
        },
        userList : function() {
            var url = baseURL + "users/";
            return $http.get(url);
        }, 

        tagList : function() {
            var url = baseURL + "tags/";
            return $http.get(url);
        }
    };  
});

blogApp.controller('blogList', ['$scope', 'apiFactory', '$routeParams', function($scope, apiFactory, $routeParams) {
    apiFactory.blogList($routeParams.filter, $routeParams.filterId).success(function(response) {
      posts = new Array();
      featured = new Array();
      for (posts_index in response) {
        if(response[posts_index].featured) {
          featured.push(response[posts_index]);
        } else {
          posts.push(response[posts_index]);
        }
      }
      $scope.posts = posts.reverse();
      $scope.featured = featured.reverse();
    });

    apiFactory.currentUser().success(function(response) {
      $scope.username = response.username;
    });
}]);


blogApp.controller('blogDetail', ['$scope','apiFactory', '$routeParams',  function($scope, apiFactory, $routeParams) {
    apiFactory.blogDetail($routeParams.postId).success(function(response) {
      $scope.post = response;
    });
    apiFactory.currentUser().success(function(response) {
      $scope.username = response.username;
    });
}]);

blogApp.controller('tagList', ['$scope', 'apiFactory', function($scope, apiFactory) {
    
    apiFactory.tagList().success(function(response) {
        $scope.tags = response;
    });

}]);

blogApp.controller('userList', ['$scope', 'apiFactory', function($scope, apiFactory) {
    
    apiFactory.userList().success(function(response) {
        $scope.users = response;
    });

}]);
