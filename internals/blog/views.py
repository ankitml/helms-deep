from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
#    if not request.user.is_authenticated():
#        return redirect('/admin/?next=/blog/')
    text = "hola world!!"
    template = loader.get_template('index.html')
    context = RequestContext(request, {
        'text': text,
        })
    return HttpResponse(template.render(context))
