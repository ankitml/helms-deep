from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

from api.urls import urlpatterns as api_urls
from blog.urls import urlpatterns as blog_urls
from django.contrib.auth.views import login


urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'internals.views.home', name='home'),
    (r'^accounts/login/$', login, {'template_name' : 'admin/login.html'}),
    url(r'^$', include(blog_urls)), 
    url(r'^api/', include(api_urls)),
    url(r'^edit/', include(admin.site.urls)),

)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
)

